#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>
#include <syslog.h>
#include <string.h>

#include "utils.h"
#include "config.h"
#include "server.h"



static struct option long_options[] = 
{
    {"help",    no_argument,        NULL, 'h'},
    {"verbose", no_argument,        NULL, 'v'},
    {"daemon",  no_argument,        NULL, 'd'},
    {"pid",     required_argument,  NULL, 'p'},
    {"config",  required_argument,  NULL, 'c'},
    {"syslog",  no_argument,        NULL, 'S'},
    {0, 0, 0, 0}
};

void show_help() 
{
    printf(
        "%s (crypto bubble) options:\n" 
        "-h     --help             shows this help\n" 
        "-v     --verbose          print more output\n" 
        "-d     --daemon           daemonize\n"
        "-pFILE --pid=FILE         write pid to file\n"
        "-cFILE --config=FILE      use this config file\n"
        "-S     --syslog           log to syslog instead of stdout\n"
        "\n", config.prog_name);
}

void read_args(int argc, char *argv[]) 
{
    config.verbose = 0;
    config.daemonize = 0;
    config.pid_file = NULL;
    config.config_file = NULL;
    
    if (argc >= 1)
    config.prog_name = argv[0];

    int opt = 0, 
        long_opt_ind = 0;

    // set default values
    config.logtype = LOGTYPE_STDOUT;

    while ((opt = getopt_long(argc, argv, "hvdc:p:", long_options, &long_opt_ind)) != -1) {
        switch (opt) {
            case 'h':
                show_help();
                break;
            case 'v':
                config.verbose = 1;
                break;
            case 'd':
                config.daemonize = 1;
                break;
            case 'p':
                config.pid_file = optarg;
                break;
            case 'c':
                config.config_file = optarg;
                break;
            case 'S':
                config.logtype = LOGTYPE_SYSLOG;
                break;
            default:
                puts("Unknown command option, aborting!\n");
                break;
        }
    }
}





int main(int argc, char *argv[]) 
{
    int pipe_fd[2];

    init_config();
    read_args(argc, argv);
    log_init(config.logtype);

    if (config.verbose)
        log_msg(LOG_INFO, "Starting...");

    if (config.config_file) {
        log_msg(LOG_INFO, "Reading config: %s", config.config_file);
        
        if (read_config_file() != 0)
            exit(-1);
    }

    if (config.daemonize) {
        if (config.verbose)
            log_msg(LOG_INFO, "Daemonizing");

        if (daemonize(DAEMONIZE_DEFAULT) != 0) {
            log_msg(LOG_ERR, "Failed to daemonize - terminating");
            exit(-1);
        }
    }

    if (config.pid_file) {
        log_msg(LOG_INFO, "PID file: %s", config.pid_file);
        write_pid(config.pid_file);
    }

    if (pipe(pipe_fd) == -1) {
        log_msg(LOG_ERR, "Unable to create pipe for controlling server child: %s", strerror(errno));
        exit(-1);
    }

    init_server_networking();

    if ((config.child_server_pid = fork()) == -1)
    {
        log_msg(LOG_ERR, "Failed to create server child: %s", strerror(errno));
        exit(-1);
    }

    if (config.child_server_pid == 0) {
        child_server(pipe_fd[0]);
    } else {
        parent_server(pipe_fd[1]);
    }
}

