#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <stdbool.h>
#include <syslog.h>

#include "utils.h"
#include "config.h"

const int MAX_CONFIG_SYMBOL_LEN = 80;
const int MAX_CONFIG_LINE_LEN = 127;

size_t trim_str(char *out, size_t len, const char *str)
{
    if(len == 0)
        return 0;

    const char *end;
    size_t out_size;

    // Trim leading space
    while (isspace((unsigned char)*str)) str++;

    if(*str == 0)  // All spaces?
    {
        *out = 0;
        return 1;
    }

    // Trim trailing space
    end = str + strlen(str) - 1;
    while (end > str && isspace((unsigned char)*end)) end--;
        end++;

    // Set output size to minimum of trimmed string length and buffer size minus 1
    out_size = (end - str) < len-1 ? (end - str) : len-1;

    // Copy trimmed string and add null terminator
    memcpy(out, str, out_size);
    out[out_size] = 0;

    return out_size;
}

// returns the index of the first occurance of char c,
// or 0 if str does not contain char c
int str_contains(const char* str, char c) 
{
    int i;
    for(i = 0; str[i] != '\0'; i++)
        if (str[i] == c)
            return i;

    return 0;
}

int read_str_keyval(const char* line, char* key, char* val) 
{
    int i = 0;
    int key_begin = 0, key_end = 0;
    int val_begin = 0, val_end = 0;
    int line_len = strnlen(line, MAX_CONFIG_LINE_LEN);

    while (isblank(line[i]) && i <= line_len) i++; // skip optional spaces
    if (i == line_len || !isalnum(line[i]))
        return -1;

    key_begin = i;

    while (isalnum(line[i]) && i <= line_len) i++; // take alphanumerics as key
    key_end = i;

    if (i == line_len || key_begin == key_end)
        return -1;

    while (isblank(line[i]) && i <= line_len) i++; // skip optional spaces between key and '='
    if (line[i] != '=') // there must be an equal sign
        return -1;
    i++;

    if (i >= line_len)
        return -1;

    while (isblank(line[i]) && i <= line_len) i++; // skip optional spaces
    if (i >= line_len)
        return -1;

    if (line[i] == '"') 
    {
        val_begin = ++i;
        while (line[i] != '"' && i <= line_len) i++;
        if ((i == line_len && line[i] != '"') || (val_end == val_begin))
            return -1;

        val_end = i - 1;
    } 
    else 
    {
        val_begin = i;
        while (!isblank(line[i]) && i <= line_len) i++; // take anything that is not blank as value
        val_end = i;
    }
    if (i < line_len) 
    {
        i++;
        while (isblank(line[i]) && i <= line_len) i++;
        if (!isblank(line[i]) && i < line_len)
            return -1; // there is something more than just a "key = val" on this line
    }

    
    int key_len = key_end - key_begin;
    int val_len = val_end - val_begin;

    strncpy(key, &line[key_begin], key_len);
    strncpy(val, &line[val_begin], val_len);

    return 0;
}

void init_config() 
{
    config.terminating = 0;
    // config.prog_name is set in main()
    config.child_server_pid = -1;
    config.logtype = LOGTYPE_STDOUT;
    config.verbose = 0;
    config.daemonize = 0;
    config.pid_file = NULL;
    config.config_file = NULL;

    config.port = 5555;
}

int read_config_file() 
{
    FILE *fd;
    char str[MAX_CONFIG_LINE_LEN];
    char line[MAX_CONFIG_LINE_LEN];

    if (!config.config_file)
        return 0;

    fd = fopen(config.config_file, "r");
    if (!fd) 
    {
        log_msg(LOG_ERR, "Failed to open config file: %s", strerror(errno));
        return -1;
    }

    int linenum = 0;
    while (true) {
        linenum++;

        if (!fgets(str, MAX_CONFIG_LINE_LEN, fd))
            return 0;

        if (strlen(str) >= MAX_CONFIG_LINE_LEN - 1) {
            log_msg(LOG_ERR, "Line %d: Line is too long.", linenum);
            return -1;
        }

        trim_str(line, strnlen(str, MAX_CONFIG_LINE_LEN), str);
        if(line[0] == '\0' || line[0] == '#') {
            continue;
        }

        char* key = malloc(MAX_CONFIG_SYMBOL_LEN);
        char* value = malloc(MAX_CONFIG_SYMBOL_LEN);
        if (read_str_keyval(line, key, value) != 0) 
        {
            log_msg(LOG_ERR, "Config file %s, line %d, syntax error: \"%s\"", config.config_file, linenum, line);
            continue;
        }

        if (str_contains(value, ' ')) {
            log_msg(LOG_INFO, "set %s = \"%s\"", key, value);
        } else {
            log_msg(LOG_INFO, "set %s = %s", key, value);
        }

        if (strcmp("pid", key)) {
            if (config.pid_file)
                free(config.pid_file);

            config.pid_file = value;
        }

    }
    return 0;
}

