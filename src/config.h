#ifndef CBUBBLE_CONFIG
#define CBUBBLE_CONFIG

static struct config_struct 
{
    // true if this program is currently in the process of terminating
    int     terminating;

    char*   prog_name;
    int     child_server_pid;
    int     logtype;
    int     verbose;
    int     daemonize;
    char*   pid_file;
    char*   config_file;

    int     port;

} config;

void init_config();
int read_config_file();

#endif

