#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include <sys/wait.h>

#include "config.h"
#include "utils.h"
#include "server.h"

int udp_fd = 0;
int tcp_fd = 0;

int init_server_networking()
{
    struct sockaddr_in saddr;

    if ((udp_fd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        log_msg(LOG_ERR, "Unable to open udp socket: %s", strerror(errno));
        return -1;
    }

    memset(&saddr, 0, sizeof(struct sockaddr_in));
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    saddr.sin_port = htons(config.port);

    if ((bind(udp_fd, (struct sockaddr *) &saddr, sizeof(saddr))) == -1) {
        log_msg(LOG_ERR, "Unable to bind udp socket to port %d: %s", config.port, strerror(errno));
        close(udp_fd);
        return -1;
    }

    if ((tcp_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        log_msg(LOG_ERR, "Unable to open tcp socket: %s", strerror(errno));
        return -1;
    }

    memset(&saddr, 0, sizeof(struct sockaddr_in));
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    saddr.sin_port = htons(config.port);

    if ((bind(tcp_fd, (struct sockaddr *) &saddr, sizeof(saddr))) == -1) {
        log_msg(LOG_ERR, "Unable to bind tcp socket to port %d: %s", config.port, strerror(errno));
        close(udp_fd);
        close(tcp_fd);
        return -1;
    }

    return 0;
}



int child_server(int read_pipe_fd) 
{
    setup_child_server_signals();

    log_msg(LOG_INFO, "Child entering sleep");
    sleep(30);
    log_msg(LOG_INFO, "Child exiting sleep");
    exit(33);
}

int parent_server(int write_pipe_fd) 
{
    setup_signal_handlers();
    log_msg(LOG_NOTICE, "Server sleeping...");
    while (true) {
        log_msg(LOG_INFO, "Heartbeat");
        sleep(60);
    }
    exit(0);
}