#ifndef CBUBBLE_SERVER
#define CBUBBLE_SERVER

// might require privileges to execute
int init_server_networking();

int child_server(int read_pipe_fd);
int parent_server(int write_pipe_fd);

#endif