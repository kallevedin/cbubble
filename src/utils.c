#include <sys/stat.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <syslog.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>

#include "config.h"
#include "utils.h"





void close_all_fds() 
{
    int fd;
    int maxfd = sysconf(_SC_OPEN_MAX);

    if (maxfd == -1)
        maxfd = 8192;

    for (fd = 0; fd < maxfd; fd++) {
        close(fd);
    }
}





// daemonizes the program. returns zero on success.
int daemonize(int flags) 
{
    int fd;

    switch (fork()) {
        case -1:    return -1;
        case 0:     break;
        default:    _exit(EXIT_SUCCESS);
    }

    if (setsid() == -1) {
        log_msg(LOG_ERR, "Failed to become session leader: %s", strerror(errno));
        return -1;
    }

    switch (fork()) {
        case -1:    return -1;
        case 0:     break;
        default:    _exit(EXIT_SUCCESS);
    }

    if (!(flags & NO_UMASK0))
        umask(0);

    if (!(flags & NO_CHDIR)) {
        if(chdir("/") == -1) {
            log_msg(LOG_ERR, "Failed to chdir to the root dir (/): %s", strerror(errno));
            return -1;
        }
    }

    if (!(flags & NO_CLOSE_FILES)) {
        close_all_fds();
    }

    if (!(flags & NO_REOPEN_STD_FDS)) {
        close(STDIN_FILENO);
        fd = open("/dev/null", O_RDWR);

        if (fd != STDIN_FILENO) {
            log_msg(LOG_ERR, "Failed to open(/dev/null): %s", strerror(errno));
            return -1;
        }
        if (dup2(STDIN_FILENO, STDOUT_FILENO) == -1) {
            log_msg(LOG_ERR, "Failed to dup2(/dev/null, stdin): %s", strerror(errno));
            return -1;
        }
        if (dup2(STDIN_FILENO, STDERR_FILENO) == -1)
        {
            log_msg(LOG_ERR, "Failed to dup2(/dev/null, stderr): %s", strerror(errno));
            return -1;
        }
    }
    return 0;
}






void (*logger)(int prio, const char* format, va_list ap);

void stdout_log_wrapper(int prio, const char* format, va_list ap)
{
    const int BUFSIZE = 512;
    char strbuf[BUFSIZE];
    bzero(strbuf, BUFSIZE);

    vsnprintf(strbuf, BUFSIZE, format, ap);
    puts(strbuf);
    fsync(STDOUT_FILENO);
}

void log_init(int logtype) 
{
	if (logtype == LOGTYPE_STDOUT) {
		logger = &stdout_log_wrapper;
	} else if (logtype == LOGTYPE_SYSLOG) {
		logger = &vsyslog;
        openlog(config.prog_name, LOG_PID, LOG_USER);
	}
}

// logs the message to whatever we are using for logging
void log_msg(int prio, const char* format, ...) 
{
	va_list ap;
	va_start(ap, format);
	logger(prio, format, ap);
	va_end(ap);
}





void write_pid(char* filepath) 
{
	FILE *fd;
	fd = fopen(filepath, "w");
	fprintf(fd, "%d", (int)getpid());
	fclose(fd);
}



static void signal_handler(int sig) 
{
    // the errno flag is a global varaible that we dont want to overwrite
    int saved_errno = 0;
    saved_errno = errno;

    log_msg(LOG_NOTICE, "%s", strsignal(sig));

    if (sig == SIGINT || sig == SIGTERM) {
        config.terminating = 1;
        kill(config.child_server_pid, SIGTERM);
        exit(0);
    }
    else if (sig == SIGHUP) {
        log_msg(LOG_INFO, "Reloading config file");
        read_config_file();
    }
    else if (sig == SIGCHLD) {
        if (!config.terminating)
            log_msg(LOG_NOTICE, "Child server process died unexpectedly - terminating");

        waitpid (-1, NULL, WNOHANG);

        if (config.terminating) {
            exit(0);
        } else {
            exit(-1);
        }
    }

    errno = saved_errno;
}

// returns zero if successful
int setup_signal_handlers() 
{
    struct sigaction sa;

    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;

    if ((sigaction(SIGINT, &sa, NULL) == -1)
        || (sigaction(SIGTERM, &sa, NULL) == -1)
        || (sigaction(SIGHUP, &sa, NULL) == -1)
        || (sigaction(SIGCHLD, &sa, NULL) == -1))
    {
        log_msg(LOG_ERR, "Unable to register interrupt handlers: %s", strerror(errno));
        return -1;        
    }
	return 0;
}

int setup_child_server_signals() 
{
    if (!config.daemonize) {
        // if we are still connected to a terminal we don't want the child to die just because the terminal hangs up
        if ((signal(SIGHUP, SIG_IGN)) == SIG_ERR) {
            log_msg(LOG_ERR, "Could not ignore HUP signal in child: %s", strerror(errno));
            return -1;
        }
    }
    return 0;
}
