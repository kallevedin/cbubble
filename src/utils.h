#ifndef CBUBBLE_UTILS
#define CBUBBLE_UTILS

#define DAEMONIZE_DEFAULT       0 // do everything
#define NO_CHDIR                1
#define NO_CLOSE_FILES          2
#define NO_REOPEN_STD_FDS       4
#define NO_UMASK0               8

int daemonize(int flags);



#define LOGTYPE_STDOUT	1
#define LOGTYPE_SYSLOG	2

void log_init();
void log_init();
void log_msg(int prio, const char* format, ...);

void write_pid(char* filepath);

int setup_signal_handlers();
int setup_child_server_signals();

// closes all open file descriptors
void close_all_fds();

#endif
